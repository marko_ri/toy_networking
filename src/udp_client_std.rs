use std::{io, net::UdpSocket, str};

type GenericResult<T> = Result<T, Box<dyn std::error::Error + Send + Sync + 'static>>;

fn main() -> GenericResult<()> {
    let socket = UdpSocket::bind("127.0.0.1:9090")?;
    socket.connect("127.0.0.1:8080")?;
    loop {
        let mut user_input = String::new();
        io::stdin().read_line(&mut user_input)?;
        socket.send(user_input.as_bytes())?;

        let mut server_resp = [0; 64];
        socket.recv_from(&mut server_resp)?;
        println!("{}", str::from_utf8(&server_resp)?);
    }
}
