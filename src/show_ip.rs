use std::{
    env,
    net::{SocketAddr, ToSocketAddrs},
    process,
};

fn main() {
    const PORT: u16 = 80;
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        eprintln!("usage: showip hostname");
        process::exit(1);
    }

    // to_socket_addrs() is calling getaddrinfo() (?)
    let addrs: Vec<SocketAddr> = format!("{}:{}", args[1], PORT)
        .to_socket_addrs()
        .expect("unable to resolve hostname")
        .collect();

    println!("IP addresses for {}:\n", args[1]);
    for addr in addrs {
        match addr {
            SocketAddr::V4(addr_v4) => println!("v4: {}", addr_v4.ip()),
            SocketAddr::V6(addr_v6) => println!("v6: {}", addr_v6.ip()),
        };
    }
}
