use nix::{
    sys::socket::{self, connect, recv, AddressFamily, MsgFlags, SockFlag, SockType, SockaddrIn},
    unistd::close,
};
use std::{env, os::fd::AsRawFd, str::FromStr};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    const MAX_DATA_SIZE: usize = 100;
    let mut buf = [0; MAX_DATA_SIZE];
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        return Err("usage: client hostname".into());
    }

    let sock = socket::socket(
        AddressFamily::Inet,
        SockType::Stream,
        SockFlag::empty(),
        None,
    )?;

    let sock_addr = SockaddrIn::from_str(&format!("{}:8080", args[1]))?;
    println!("client: connecting to {sock_addr}");
    connect(sock.as_raw_fd(), &sock_addr)?;

    let numbytes = recv(sock.as_raw_fd(), &mut buf, MsgFlags::empty())?;
    let result = String::from_utf8(buf[..numbytes].to_vec())?;
    println!("client received: {result}");

    close(sock.as_raw_fd())?;
    Ok(())
}
