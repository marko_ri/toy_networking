use nix::sys::{
    select::{select, FdSet},
    time::{TimeVal, TimeValLike},
};
use std::os::fd::AsRawFd;

fn main() {
    let stdin = std::io::stdin();
    let mut read_fds = FdSet::new();
    read_fds.insert(&stdin);
    let mut timeout = TimeVal::milliseconds(2500);

    select(
        stdin.as_raw_fd() + 1,
        &mut read_fds,
        None,
        None,
        &mut timeout,
    )
    .expect("select");

    if read_fds.contains(&stdin) {
        println!("A key was pressed");
    } else {
        println!("Timed out");
    }
}
