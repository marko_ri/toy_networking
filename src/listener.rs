use nix::{
    sys::socket::{self, bind, recvfrom, AddressFamily, SockFlag, SockType, SockaddrIn},
    unistd::close,
};
use std::{os::fd::AsRawFd, str::FromStr};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    const PORT: u16 = 4950;
    const MAX_BUF_LEN: usize = 100;
    let mut buf = [0; MAX_BUF_LEN];

    let sock = socket::socket(
        AddressFamily::Inet,
        SockType::Datagram,
        SockFlag::empty(),
        None,
    )?;

    let sock_addr = SockaddrIn::from_str(&format!("0.0.0.0:{}", PORT))?;
    bind(sock.as_raw_fd(), &sock_addr)?;

    println!("listener: waiting to recvfrom...");
    let (num_bytes, sender) = recvfrom::<SockaddrIn>(sock.as_raw_fd(), &mut buf)?;

    if let Some(addr) = sender {
        println!("listener: got packet from {addr}");
    }
    println!("listener: packet is {num_bytes} long");
    let result = String::from_utf8(buf[..num_bytes].to_vec())?;
    println!("listener: packet contains: {result}");

    close(sock.as_raw_fd())?;
    Ok(())
}
