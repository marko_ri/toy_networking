use nix::{
    sys::socket::{self, sendto, AddressFamily, MsgFlags, SockFlag, SockType, SockaddrIn},
    unistd::close,
};
use std::{env, os::fd::AsRawFd, str::FromStr};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    const PORT: u16 = 4950;
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        return Err("usage: talker hostname message".into());
    }

    let sock = socket::socket(
        AddressFamily::Inet,
        SockType::Datagram,
        SockFlag::empty(),
        None,
    )?;

    let sock_addr = SockaddrIn::from_str(&format!("{}:{}", args[1], PORT))?;
    let num_bytes = sendto(
        sock.as_raw_fd(),
        args[2].as_bytes(),
        &sock_addr,
        MsgFlags::empty(),
    )?;
    println!("talker: sent {num_bytes} to {sock_addr}");

    close(sock.as_raw_fd())?;
    Ok(())
}
