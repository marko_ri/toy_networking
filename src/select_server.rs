use nix::{
    sys::{
        select::{select, FdSet},
        socket::{
            self, accept, bind, getpeername, listen, recv, send, setsockopt, sockopt,
            AddressFamily, MsgFlags, SockFlag, SockType, SockaddrIn,
        },
    },
    unistd::close,
};
use std::{
    collections::HashMap,
    os::fd::{AsFd, AsRawFd, BorrowedFd, OwnedFd},
    str::FromStr,
};
type GenericResult<T> = Result<T, Box<dyn std::error::Error + Send + Sync + 'static>>;

fn get_listener_socket() -> GenericResult<OwnedFd> {
    const PORT: u16 = 9034;

    let sock = socket::socket(
        AddressFamily::Inet,
        SockType::Stream,
        SockFlag::empty(),
        None,
    )?;
    setsockopt(&sock, sockopt::ReuseAddr, &true)?;

    let sock_addr = SockaddrIn::from_str(&format!("127.0.0.1:{}", PORT))?;
    bind(sock.as_raw_fd(), &sock_addr)?;

    listen(&sock, 10)?;

    Ok(sock)
}

fn main() -> GenericResult<()> {
    let mut buf;
    let mut master = HashMap::new();

    let listener_owned = get_listener_socket()?;
    let listener_raw_fd = listener_owned.as_raw_fd();
    master.insert(listener_raw_fd, listener_owned.as_fd());
    let mut fd_max = listener_raw_fd;

    loop {
        // TODO: not cool
        let master_cloned = master.clone();
        let mut read_fds = master_cloned.values().fold(FdSet::new(), |mut acc, fd| {
            acc.insert(fd);
            acc
        });
        buf = [0; 64];

        let ready_num = select(fd_max + 1, &mut read_fds, None, None, None)?;
        println!("Num of Ready FD: {ready_num}");

        for i in 0..=fd_max {
            let i_fd = unsafe { BorrowedFd::borrow_raw(i) };
            if read_fds.contains(&i_fd) {
                if i == listener_raw_fd {
                    // If listener is ready to read, handle new connection
                    let sock = accept(listener_raw_fd)?;
                    let sock_fd = unsafe { BorrowedFd::borrow_raw(sock) };
                    master.insert(sock, sock_fd);
                    if sock > fd_max {
                        fd_max = sock;
                    }
                    let sock_addr = getpeername::<SockaddrIn>(sock)?;
                    println!("new connection from {sock_addr} on socket {sock}");
                } else {
                    // If not the listener, we're just a regular client
                    match recv(i, &mut buf, MsgFlags::empty()) {
                        Ok(0) => {
                            // Got error or connection closed by client
                            println!("socket {i} hung up");
                            close(i)?;
                            master.remove(&i);
                        }
                        Ok(_) => {
                            // We got some good data from a client
                            for j in 0..=fd_max {
                                if master.contains_key(&j) && j != listener_raw_fd && j != i {
                                    send(j, &buf, MsgFlags::empty())?;
                                }
                            }
                        }
                        _ => {
                            close(i)?;
                            master.remove(&i);
                            panic!("recv failed");
                        }
                    }
                }
            }
        }
    }
}
