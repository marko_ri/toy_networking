use nix::poll::{poll, PollFd, PollFlags};
use std::{
    io,
    os::fd::{AsFd, AsRawFd},
};

fn main() {
    let stdin = io::stdin();
    let mut pfds = [PollFd::new(&stdin, PollFlags::POLLIN)];
    println!("Hit RETURN or wait 2.5 seconds for timeout");
    let num_events = poll(&mut pfds, 2500).expect("poll");
    if num_events == 0 {
        println!("Poll timed out");
    } else {
        match pfds[0].revents() {
            Some(PollFlags::POLLIN) => println!(
                "File descriptor {} is ready to read",
                pfds[0].as_fd().as_raw_fd()
            ),
            Some(event) => println!("Unexpected event occured: {event:?}"),
            None => panic!("revents failed"),
        }
    }
}
