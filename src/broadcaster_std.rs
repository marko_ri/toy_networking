use std::env;
use std::net::UdpSocket;

type GenericResult<T> = Result<T, Box<dyn std::error::Error + Send + Sync + 'static>>;

fn main() -> GenericResult<()> {
    const PORT: u16 = 4950;
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        return Err("usage: broadcaster_std hostname message".into());
    }

    let socket = UdpSocket::bind(format!("{}:9090", args[1]))?;
    socket.set_broadcast(true)?;

    let num_bytes = socket.send_to(args[2].as_bytes(), format!("{}:{}", args[1], PORT))?;

    println!("broadcaster_std: sent {num_bytes}");

    Ok(())
}
