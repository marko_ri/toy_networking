use std::io::{self, BufRead, BufReader, Write};
use std::net::TcpStream;

type GenericResult<T> = Result<T, Box<dyn std::error::Error + Send + Sync + 'static>>;

fn main() -> GenericResult<()> {
    let mut stream = TcpStream::connect("127.0.0.1:8080")?;
    loop {
        let mut user_input = String::new();
        io::stdin().read_line(&mut user_input)?;
        stream.write_all(user_input.as_bytes())?;

        let mut reader = BufReader::new(&stream);
        let mut server_resp = String::new();
        reader.read_line(&mut server_resp)?;
        if server_resp.is_empty() {
            return Err("server gone".into());
        }
        println!("{server_resp}");
    }
}
