use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use std::thread;

type GenericResult<T> = Result<T, Box<dyn std::error::Error + Send + Sync + 'static>>;

fn handle_connection(mut stream: TcpStream) -> GenericResult<()> {
    println!("Incoming connection from: {}", stream.peer_addr()?);
    let mut buf = [0; 64];
    loop {
        let bytes_read = stream.read(&mut buf)?;
        if bytes_read == 0 {
            return Err("client gone".into());
        }
        stream.write_all(&buf[..bytes_read])?;
    }
}

fn main() -> GenericResult<()> {
    let listener = TcpListener::bind("0.0.0.0:8080")?;
    //let pool = ThreadPool::new(4);

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                thread::spawn(move || {
                    handle_connection(stream)
                        .unwrap_or_else(|e| eprintln!("handle_connection: {e}"));
                });
            }
            Err(e) => eprintln!("Failed stream: {e}"),
        }
    }

    Ok(())
}
