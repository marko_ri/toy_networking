use nix::{
    poll::{poll, PollFd, PollFlags},
    sys::socket::{
        self, accept, bind, getpeername, listen, recv, send, setsockopt, sockopt, AddressFamily,
        MsgFlags, SockFlag, SockType, SockaddrIn,
    },
    unistd::close,
};
use std::{
    collections::HashMap,
    os::fd::{AsFd, AsRawFd, BorrowedFd, OwnedFd},
    str::FromStr,
};

type GenericResult<T> = Result<T, Box<dyn std::error::Error + Send + Sync + 'static>>;

fn get_listener_socket() -> GenericResult<OwnedFd> {
    const PORT: u16 = 9034;

    let sock = socket::socket(
        AddressFamily::Inet,
        SockType::Stream,
        SockFlag::empty(),
        None,
    )?;
    setsockopt(&sock, sockopt::ReuseAddr, &true)?;

    let sock_addr = SockaddrIn::from_str(&format!("127.0.0.1:{}", PORT))?;
    bind(sock.as_raw_fd(), &sock_addr)?;

    listen(&sock, 10)?;

    Ok(sock)
}

fn main() -> GenericResult<()> {
    let mut buf;
    let mut master = HashMap::new();

    let listener_owned = get_listener_socket()?;
    let listener = listener_owned.as_raw_fd();
    master.insert(listener, listener_owned.as_fd());

    loop {
        // TODO: not cool
        let master_cloned = master.clone();
        let mut poll_fds = master_cloned
            .values()
            .map(|fd| PollFd::new(fd, PollFlags::POLLIN))
            .collect::<Vec<PollFd>>();
        buf = [0; 64];

        poll(&mut poll_fds, -1)?;
        println!("EVENTS: {:?}", poll_fds[0].revents());

        // Run through the existing connections looking for data to read
        for i in 0..poll_fds.len() {
            let fd = poll_fds[i];
            // Check if someone's ready to read
            if fd.revents().unwrap().contains(PollFlags::POLLIN) {
                if fd.as_fd().as_raw_fd() == listener {
                    // If listener is ready to read, handle new connection
                    let sock = accept(listener)?;
                    let b_fd = unsafe { BorrowedFd::borrow_raw(sock) };
                    master.insert(sock, b_fd);
                    let s_addr = getpeername::<SockaddrIn>(sock)?;
                    println!("new connection from {s_addr} on socket {sock}");
                } else {
                    // If not the listener, we're just a regular client
                    match recv(fd.as_fd().as_raw_fd(), &mut buf, MsgFlags::empty()) {
                        Ok(0) => {
                            // Got error or connection closed by client
                            println!("socket {} hung up", fd.as_fd().as_raw_fd());
                            close(fd.as_fd().as_raw_fd())?;
                            master.remove(&fd.as_fd().as_raw_fd());
                        }
                        Ok(_) => {
                            // We got some good data from a client
                            for p_fd in poll_fds.iter() {
                                let dest_fd = p_fd.as_fd().as_raw_fd();
                                if dest_fd != listener && dest_fd != fd.as_fd().as_raw_fd() {
                                    send(dest_fd, &buf, MsgFlags::empty())?;
                                }
                            }
                        }
                        _ => {
                            close(fd.as_fd().as_raw_fd())?;
                            master.remove(&fd.as_fd().as_raw_fd());
                            panic!("recv failed");
                        }
                    }
                }
            }
        }
    }
}
