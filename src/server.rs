// TODO: network programming in rust

use nix::{
    errno::Errno,
    sys::{
        signal::{sigaction, SaFlags, SigAction, SigHandler, SigSet, Signal},
        socket::{
            self, accept, bind, getpeername, listen, send, setsockopt, sockopt, AddressFamily,
            MsgFlags, SockFlag, SockType, SockaddrIn,
        },
        wait::{waitpid, WaitPidFlag, WaitStatus},
    },
    unistd::{close, fork, ForkResult},
};
use std::{os::fd::AsRawFd, str::FromStr};

extern "C" fn sigchld_handler(_signal: libc::c_int) {
    while let Ok(WaitStatus::StillAlive) = waitpid(None, Some(WaitPidFlag::WNOHANG)) {}
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let sock = socket::socket(
        AddressFamily::Inet,
        SockType::Stream,
        SockFlag::empty(),
        None,
    )?;

    setsockopt(&sock, sockopt::ReuseAddr, &true)?;

    let localhost = SockaddrIn::from_str("127.0.0.1:8080")?;
    bind(sock.as_raw_fd(), &localhost)?;

    let backlog = 10;
    listen(&sock, backlog)?;

    let handler = SigHandler::Handler(sigchld_handler);
    let sa = SigAction::new(handler, SaFlags::empty(), SigSet::empty());
    unsafe {
        sigaction(Signal::SIGCHLD, &sa)?;
    }

    println!("server: waiting for connections...");
    loop {
        match accept(sock.as_raw_fd()) {
            Err(Errno::EINTR) => continue,
            Ok(session_sock) => {
                if let Ok(saddr) = getpeername::<SockaddrIn>(session_sock) {
                    println!("server: got connection from {saddr}");
                }

                match unsafe { fork() }? {
                    ForkResult::Child => {
                        close(sock.as_raw_fd())?;
                        send(session_sock, "Hello World!".as_bytes(), MsgFlags::empty())?;
                        close(session_sock)?;
                        return Ok(());
                    }
                    _ => close(session_sock)?,
                }
            }
            _ => return Err("server: accept failed".into()),
        }
    }
}
