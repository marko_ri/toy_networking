use std::net::UdpSocket;
use std::thread;

type GenericResult<T> = Result<T, Box<dyn std::error::Error + Send + Sync + 'static>>;

fn main() -> GenericResult<()> {
    let socket = UdpSocket::bind("0.0.0.0:8080")?;

    loop {
        let mut buf = [0; 64];
        let socket_cloned = socket.try_clone()?;
        match socket.recv_from(&mut buf) {
            Ok((_amt, src)) => {
                thread::spawn(move || {
                    println!("Incoming connection from: {src}");
                    socket_cloned
                        .send_to(&buf, src)
                        .expect("failed to send a response");
                });
            }
            Err(e) => eprintln!("failed datagram: {e}"),
        }
    }
}
